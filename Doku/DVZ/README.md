# **DOKU K8s Installation Nextcloud im DVZ**

**Disclaimer:** Die hier bereitgestellten Dateien sind Beispieleinstellungen für das Erstellen der Dokumentation. Angegebene Kennwörter sind erdacht. Sie dienen lediglich zur Veranschaulichung. Es handelt sich hier um eine erste Version der example.values.yaml Datei. Sobald Änderungen vorgenommen werden, wird diese auch entsprechend aktualisiert.


## **Softwarestack**

 Hier eine Tabelle der eingesetzten Software für die Installation
|  |  |
|--|--|
|  Kubernetes Version| 1.20.8 |
|Kubernetes Distributor|Tanzu Kubernetes Grid Integreatet Edition|
| CI/CD Tool | Azure DevOps
|Registry| Dev: Artifactory Prod: Harbor|
| Security Scanner | xRay & Trivy  |
| Policy Validator | Kyverno 1.5.1  |
| Container Runtime | Docker & Skopeo  |

## **Kyverno Policy Verstöße**


Nach dem auktuellen [**Richtlienien**](https://gitlab.opencode.de/ig-bvc/richtlinien) Release [**v0.0.2-alpha**](https://gitlab.opencode.de/ig-bvc/richtlinien/-/releases/v0.0.2-alpha) der IG BvC verstößt Nextcloud gegen folgenden Policies:

 - **require-non-root-groups:**

	`autogen-check-fsGroup: 'validation error: Changing to group ID less than 1000 is disallowed. The field spec.securityContext.fsGroup must be empty or less than 1000. . Rule autogen-check-fsGroup failed at path /spec/template/spec/securityContext/fsGroup/'`

 - **require-requests-limits:**

    `hereautogen-validate-resources: 'validation error: CPU and memory resource requests and limits are required. Rule autogen-validate-resources failed at path /spec/template/spec/containers/0/resources/requests/'`

 - **require-ro-rootfs:**

	`autogen-validate-readOnlyRootFilesystem: 'validation error: Root filesystem must be read-only. Rule autogen-validate-readOnlyRootFilesystem failed at path /spec/template/spec/containers/0/securityContext/'`

 - **require-run-as-non-root:**
 
	`autogen-check-containers: 'validation error: Running as root is not allowed. The fields spec.securityContext.runAsNonRoot, spec.containers[*].securityContext.runAsNonRoot, and spec.initContainers[*].securityContext.runAsNonRoot must be `true`. Rule autogen-check-containers[0] failed at path /spec/template/spec/securityContext/runAsNonRoot/. Rule autogen-check-containers[1] failed at path /spec/template/spec/containers/0/securityContext/.'`

##  **Helm Chart und Images lokal verfügbar machen**

Im DVZ haben wir für die Images und Helm Chart Release Pipelines im unserem CI/CD Tool „Azure DevOps“ eingerichtet. Somit werden per Knopfdruck die aktuellsten Images und Helmcharts geladen, zusätzlich auf Schwachstellen gescannt und in die entsprechende Registry hochgeladen.

 Wer sowas noch nicht eingerichtet hat kann sich hier diese kleine Anleitung anschauen wie man das alles manuell macht:

 - **o4oe Helm Chart Repo dem lokalen Repo hinzufügen**

	    $ helm repo add nextcloud https://gitlab.opencode.de/api/v4/projects/63/packages/helm/stable
 
 - **Lokales Repository aktualisieren**
 
		$ helm repo update

 - **Helm Chart herunterladen**

		$ helm pull nextcloud/nextcloud

 - **Helm Repo Löschen**

    Nun liegt das Helm als .tgz Datei im aktuellen Verzeichnis. Dieses Helm kann nun über die Upload Funktion (der eigenen Registry) der lokalen Registry hinzugefügt werden. 
Ist dieser Schritt erfolgreich abgeschlossen, kann das o4eo Helm Repo wieder gelöscht werden: 

		$ helm repo list 
		$ helm repo remove nextcloud

 - **Privates Helm Chart dem lokalen Repository hinzufügen**

	Im Folgenden ist mv-harbor.esx.portal.cn-mv.de unsere lokale Harbor Registry. Diese Adresse verwenden wir in den kommenden Beispielen. Das Nextcloud Projekt/Repository heißt in unserem Beispiel nextcloud. 

 
		$ helm repo add nextcloud https://mv-harbor.esx.portal.cn-mv.de/chartrepo/nextcloud --username ‘robot$nextcloud’ –password ‘Start-1234’ 
		$ helm repo update


 - **Benötigte Images ermitteln**

	Da die zugehörigen Images noch fehlen, müssen diese ebenfalls aus dem o4eo Repo herunter geladen werden, und in die eigene Registry gepusht werden.
Feststellen, welche Images benötigt werden:
			
		$ helm template nextcloud/nextcloud | grep 'image:'

	Es wird ein Image benötigt:

   `nextcloud:21.0.4-apache`

	**Hinweis**: Das im HelmChart angegebene Image wird nicht verwendet. Statdessen verwenden wir die neuere version `nextcloud:22.2.0-apache`.

 - **Image herunterladen und in die eigene private Registry pushen**

	 Mittels Docker das Image herunterladen:

		$ sudo docker pull registry.opencode.de/ig-bvc/demo-apps/nextcloud/nextcloud:22.2.0-apache

	Image für die lokale Registry vorbereiten (taggen) 
		
		$ sudo docker tag registry.opencode.de/ig-bvc/demo-apps/nextcloud/nextcloud:22.2.0-apache mv-harbor.esx.portal.cn-mv.de/nextcloud/nextcloud:22.2.0-apache 

	Image in die lokale Registry pushen 
	
		$ docker login mv-harbor.esx.portal.cn-mv.de
		username: **********
		password: **********

        $ sudo docker push mv-harbor.esx.portal.cn-mv.de/nextcloud/nextcloud:22.2.0-apache


___
**Damit ist der erste Schritt abgeschlossen – sowohl Helm als auch die Images sind in der lokalen Registry abgelegt. Eine Installation aus der o4oe Registry ist nicht mehr notwendig.
Nachdem man nun Zugang zur privaten Registry (Images und dem Helm Charts) hat, kann man beginnen, den Cluster vorzubereiten.**


## K8s Cluster vorbereiten

 - **Neuen Namespce erstellen**
	
		$ kubectl create namespace nextcloud
	Zum namespace wechseln:
	
		$ kubectl config set-context --current –namespace nextcloud
		
 - **TLS Zertifikat installieren**
 
	Dafür wurde bei uns ein Zertifikat im base64 Format von der zuständigen Abteilung beauftrag. Dieses haben wir dann nur noch „base64 kodiert“ und als .yaml in eine datei geschrieben. (Siehe Datei `example.dvs.dvz.de-wildcard.yaml`)Es lässt sich einfacher eine datei zu deployen, als lange befehle zu schreiben mit der Information.
Danach deployt man diese Datei: 

		$ kubectl apply –f example.dvs.dvz.de-wildcard.yaml

 - **Pullsecret erstellen**

    Da wir eine private registry haben müssen wir Kubernetes die zugangsdaten zu unsere Registry geben. Dafür wird ein Pullsecret benötigt. Hier den befehl wenn man direkt von der o4eo registry pullen möchte ansonsten trägt man seine eigene registry ein:

	    $ kubectl create secret dvs_repo_pullsecret docker-registry --docker-server=' mv-harbor.esx.portal.cn-mv.de ' --docker-username= robot$nextcloud ' --docker-password='Start-1234' 
		

 - **Eigene value.yaml datei erstellen**
	
	Generell empfiehlt es sich immer mit helm separate values.yaml dateien zu erstellen. So muss man das originale Helmchart nicht verändern, sondern man überschreibt beim Deployen die bestehende Werte mit seinen Eigenen. Zu sehen in der Datei `example.values.yaml`
Die yaml Datei enthält in der Regel nur die Werte, die verändert werden sollen. Mögliche konfigurationsoptionen kann man der originalen values.yaml entnehmen.

## Nextcloud Installieren

	$ helm install nextcloud nextcloud/nextcloud –values example.values.yaml --namespace=nextcloud

**Nextcloud direkt aus dem o4oe Repo Instalieren**

Offizielle Dokumentation: [https://github.com/nextcloud/helm/tree/master/charts/nextcloud](https://github.com/nextcloud/helm/tree/master/charts/nextcloud)

Aus gitlab.opencode.de gespiegelte Helm-Charts für die Installation nutzen:

```
$ helm repo add nextcloud https://gitlab.opencode.de/api/v4/projects/63/packages/helm/stable
$ helm repo update
```

Mit gitlab.opencode.de nextcloud Image starten. Im Ziel-Namespace muss ein Pull-Secret für registry.opencode.de vorhanden sein.

```
$ kubectl create secret docker-registry --docker-server='registry.opencode.de' --docker-username='pull-images-poc-until-2021-12-31' --docker-password='_zZhhf-mQq6DYkp1UtCC' registry-o4oe-access
$ helm install stable nextcloud/nextcloud --set image.repository=registry.opencode.de/ig-bvc/demo-apps/nextcloud/nextcloud,image.tag=22.2.0-fpm-alpine,image.pullSecrets='{registry-o4oe-access}'


