# Installation in OpenShift / OKD 4

Hier die Beispielinstallation mit Kustomize und helm templating auf OpenShift / OKD 4.7+.

Ziel ist eine saubere Installation in einem eigenen Namespace. 

Dabei bekommen alle Objekte Namen nach der Konvention `instancename-objectname-stage` wobei gilt:

- `instancename` -: eineindeutiger Name der Instanz
- `objectname` -: Objektname laut Basemanifest
- `stage` -: Overlay Stage: `dev, test, qm, prod`

## Vorgang:

1. Projekt anlegen und default pull-secret installieren: 

    ```shell
    oc new-project poc-nextcloud
    oc apply -f manifests/image-pull-secret.yaml
    oc apply -f manifests/default-serviceaccount.yaml
    ```

1. (Optional): Build des `nonroot` Images:

    - Erzeugen eines git-Secrets zum clonen für OKC/OCP:
        - im Gitlab: "Projekt -> Settings -> Acces Tokens" Token erzeugen mit folgenden Rechten `read_api, read_repository, read_registry`
        - Username = Token name, Passwort= Token
        - `oc create secret generic read-repo --from-literal=Username --from-literal=password=Token --type=kubernetes.io/basic-auth --dry-run=client --from-file=.gitconfig=dot-gitconfig.ini -o yaml > read-repo.yaml`
        - importieren mit `oc apply -f read-repo.yaml``
    - [./OpenShift-OKD-build-nextcloud.yaml](./OpenShift-OKD-build-nextcloud.yaml) editieren: Anpassen der git-Source
    - BuildConfig importieren und Prozess starten: `oc apply -f OpenShift-OKD-build-nextcloud.yaml`

1. Generieren der aktuellen helm Objekte aus dem helm Repo:

    ```shell
    helm repo add nextcloud https://gitlab.opencode.de/api/v4/projects/63/packages/helm/stable
    helm repo update
    helm template nextcloud nextcloud/nextcloud --values manifests/base/nextcloud-helm/helm-values.yaml > manifests/base/nextcloud-helm/helm-template.yaml 
    ```

1. Anschließend kopieren wir uns das Verzeichnis [./manifests/instancetemplate](./manifests/instancetemplate) in ein eigenes Verzeichnis (z.B.: ./manifests/pocnextcloud-dev/). Dies ist das Config Verzeichnis der Instanz!

1. Editieren zumindest der Datei `kustomization.yaml` in dem oben genannten Ordner

1. Deployen der Applikation mit: `kustomize build manifests/pocnextcloud-dev/ | oc apply -f -` oder mit einem Tool wie ArgoCD

## Löschen

Löschen der Instanz: `kustomize build manifests/pocnextcloud-dev/ | oc delete -f -`


## To Do

- Derzeit ist die Route in `manifests/base/nextcloud-helm/route.yaml` fix als OpenShift Route definiert. Hier könnte man einen Layer bauen, der dort entweder Ingress oder Route Elemente abhängig von der Kubernetes Implementierung schafft (oder man verwendet auch in OpenShift ein Ingress Objekt).
- Update Tests wurden bisher noch nicht durchgeführt => neue NextCloud Version
- Horizontal Scaling ist zu untersuchen. Derzeit werden ReadWriteOnce Volumes verwendet!
- Dokumentation des Kustomizings: helm template => kustomize configs


---
Peter Pfläging (BRZ) <peter@pflaeging.net>