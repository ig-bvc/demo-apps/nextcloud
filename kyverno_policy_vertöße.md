## **Kyverno Policy Verstöße**


Nach dem auktuellen [**Richtlienien**](https://gitlab.opencode.de/ig-bvc/richtlinien) Release [**v0.0.2-alpha**](https://gitlab.opencode.de/ig-bvc/richtlinien/-/releases/v0.0.2-alpha) der IG BvC verstößt Nextcloud gegen folgenden Policies:

 - **require-non-root-groups:**

	`autogen-check-fsGroup: 'validation error: Changing to group ID less than 1000 is disallowed. The field spec.securityContext.fsGroup must be empty or less than 1000. . Rule autogen-check-fsGroup failed at path /spec/template/spec/securityContext/fsGroup/'`

 - **require-requests-limits:**

    `hereautogen-validate-resources: 'validation error: CPU and memory resource requests and limits are required. Rule autogen-validate-resources failed at path /spec/template/spec/containers/0/resources/requests/'`

 - **require-ro-rootfs:**

	`autogen-validate-readOnlyRootFilesystem: 'validation error: Root filesystem must be read-only. Rule autogen-validate-readOnlyRootFilesystem failed at path /spec/template/spec/containers/0/securityContext/'`

 - **require-run-as-non-root:**
 
	`autogen-check-containers: 'validation error: Running as root is not allowed. The fields spec.securityContext.runAsNonRoot, spec.containers[*].securityContext.runAsNonRoot, and spec.initContainers[*].securityContext.runAsNonRoot must be `true`. Rule autogen-check-containers[0] failed at path /spec/template/spec/securityContext/runAsNonRoot/. Rule autogen-check-containers[1] failed at path /spec/template/spec/containers/0/securityContext/.'`