# Nextcloud

[Nextcloud](https://nextcloud.com/) ist eine freie Software für das Speichern von Daten auf einem Server. Auf die Daten kann der Anwender sowohl über eine Weboberfläche als auch mit Client-Applikationen zugreifen.

Es gibt ein offizielles von nextcloud.com supportedes [helm charts](https://github.com/nextcloud/helm/tree/master/charts/nextcloud).
Dieses wiederspricht der [require-run-as-non-root policy der ig-bvc](https://gitlab.opencode.de/ig-bvc/richtlinien/-/blob/master/policies/require-run-as-non-root.yaml#L29).
Daher wird nicht das [offizielle upstream image auf dockerhub](https://hub.docker.com/_/nextcloud) genutzt, sondern dieses in einem lokalen [Dockerfile](Dockerfile) entrooted und dann in [repository-imagetag-nonroot.values.yaml](repository-imagetag-nonroot.values.yaml) an helm übergeben.

## Deploy mit Helm


Nach gitlab.opencode.de gespiegelte Helm-Charts für die Installation nutzen:
```
$ helm repo add opencode-nextcloud https://gitlab.opencode.de/api/v4/projects/63/packages/helm/stable
$ helm repo update
````

Mit gitlab.opencode.de nextcloud image starten.
```
$ helm repo update
$ helm upgrade opencode-nextcloud-stable opencode-nextcloud/nextcloud --install --values repository-imagetag-nonroot.values.yaml --set nextcloud.host=nextcloud.example.org
```

Externe DB anbinden:
```
$ helm upgrade stable opencode-nextcloud/nextcloud \
    --set nextcloud.password=$APP_PASSWORD,nextcloud.host=$APP_HOST,service.type=ClusterIP,mariadb.enabled=false,externalDatabase.user=nextcloud,externalDatabase.database=nextcloud,externalDatabase.host=YOUR_EXTERNAL_DATABASE_HOST
```

## Images

### Build

Das entrootede Standardimage der ig-bvc wird mit [Dockerfile](Dockerfile) definiert und per [.gitlab-ci.yml](.gitlab-ci.yml) gebaut und ist k8s ready.

Openshiftuser können mit [OpenShift-OKD-build-nextcloud.yaml](OpenShift-OKD-build-nextcloud.yaml) das image auch Clusterlokal selbst builden.

### Security Scan Ergebnisse
Scans durchgeführt 2021-10-19 mit [trivy](https://github.com/aquasecurity/trivy).

#### docker "Offical Image" nextcloud (https://hub.docker.com/_/nextcloud)
- [nextcloud:22.2.0-apache](scan-results/2020-10-19-trivy-scan-results/trivy-scan-nextcloud-22.2.0-apache.txt)
  - Total: 402 (UNKNOWN: 0, LOW: 313, MEDIUM: 54, HIGH: 26, CRITICAL: 9)
- [nextcloud:22.2.0-fpm-alpine](scan-results/2020-10-19-trivy-scan-results/trivy-scan-nextcloud-22.2.0-fpm-alpine.txt)
  - Total: 3 (UNKNOWN: 0, LOW: 0, MEDIUM: 1, HIGH: 1, CRITICAL: 1)
- [nextcloud:22.2.0-fpm](scan-results/2020-10-19-trivy-scan-results/trivy-scan-nextcloud-22.2.0-fpm.txt)
  - Total: 365 (UNKNOWN: 0, LOW: 278, MEDIUM: 53, HIGH: 25, CRITICAL: 9)
