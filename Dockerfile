# FROM docker.io/library/nextcloud:22.2.0-apache
FROM registry.opencode.de/ig-bvc/demo-apps/nextcloud/nextcloud:22.2.0-apache

USER root

RUN chgrp -R 0 /var/log/apache2 && \
    chmod -R g=u /var/log/apache2

RUN sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf
RUN sed -i 's/VirtualHost *:80/VirtualHost *:8080/g' /etc/apache2/sites-enabled/000-default.conf


USER 1001
